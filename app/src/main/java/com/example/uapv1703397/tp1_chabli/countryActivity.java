package com.example.uapv1703397.tp1_chabli;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ParseException;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Console;

public class countryActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        // Initialise les EditText avec l'id du layout
        final EditText textCapitale = (EditText)findViewById(R.id.editCap);
        final EditText textLangue = (EditText)findViewById(R.id.editLangue);
        final EditText textMonnaie = (EditText)findViewById(R.id.editMonnaie);
        final EditText textPopulation = (EditText)findViewById(R.id.editPop);
        final EditText textSuperficie = (EditText)findViewById(R.id.editSup);

        // Recupération de l'intent et de l'information du pays envoyer par la fenêtre MainActivity
        Intent intent = getIntent();
        final String name = intent.getStringExtra("country");

        // Initialisation d'un objet CountryList
        final CountryList listCountry = new CountryList();

        // Récupération du nom de l'image correspondant au pays récupéré
        final String imageFile = listCountry.getCountry(name).getmImgFile();

        // Récupération de l'id de l'image à charger
        Resources res = this.getResources();
        int resID = res.getIdentifier(imageFile, "drawable", getPackageName());

        // Initialisation de l'image avec l'id et on affiche l'image avec l'id de l'image récupéré
        ImageView imageCountry = (ImageView)findViewById(R.id.imgCountry);
        imageCountry.setImageResource(resID);

        // Initialisation du text du pays avec le pays correspondant
        TextView textViewCountry = (TextView)findViewById(R.id.textCountry);
        textViewCountry.setText(name);

        // On récupère les valeurs par défaut de chaque donnée du pays choisie
        textCapitale.setText(listCountry.getCountry(name).getmCapital());
        textLangue.setText(listCountry.getCountry(name).getmLanguage());
        textMonnaie.setText(listCountry.getCountry(name).getmCurrency());
        textPopulation.setText(String.valueOf(listCountry.getCountry(name).getmPopulation()));
        textSuperficie.setText(String.valueOf(listCountry.getCountry(name).getmArea()));

        // Initialisation du bouton sauvegarder
        Button save = (Button)findViewById(R.id.ButtonSave);

        // Quand on clique sur save, lance le code suivant
        save.setOnClickListener(new View.OnClickListener()
        {
           @Override
           public void onClick(View view)
           {
               // initialisation de variable
               boolean verif = true;

               // INFORMATION :
               // J'ai rajouté des patterns à chaque EditText, pour les champs de type String j'ai utilisé celui-ci dans le XML de countryActivity
               // android:digits="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ- "
               // Et pour la population et la superficie celui-ci
               // android:digits="0123456789"

               // Récupération des champs
               String capitale = textCapitale.getText().toString();
               String langue = textLangue.getText().toString();
               String monnaie = textMonnaie.getText().toString();
               String population = textPopulation.getText().toString();
               String superficie = textSuperficie.getText().toString();

               // Si un des champs est vide alors la variable verif passe à false et on renvoie un message d'erreur
               if((capitale.length() == 0) || (langue.length() == 0) || (monnaie.length() == 0) || (population.length() == 0) || (superficie.length() == 0) )
               {
                   verif = false;
                   Toast.makeText(countryActivity.this, "Erreur : Impossible de laisser des champs vides", Toast.LENGTH_LONG).show();
               }

               if(verif)
               {
                   // Modification de la hashmap avec les nouvelles données
                   listCountry.getHashMap().put(name, new Country(capitale, imageFile, langue, monnaie, Integer.parseInt(population), Integer.parseInt(superficie)));

                   Toast.makeText(countryActivity.this, "Modification enregistré avec succès !", Toast.LENGTH_LONG).show();

                   // Retour à la page d'accueil
                   Intent intent = new Intent(countryActivity.this, MainActivity.class);
                   startActivity(intent);
               }
           }
        });
    }
}
